import { HookFilterSync } from '@spine/hook/HookFilterSync';
import { Logger } from '..';

export const displayNameHook = HookFilterSync.template(
  (displayName: string, parents: Logger[], path: string[], logger: Logger) => displayName,
);
