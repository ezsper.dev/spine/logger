import { HookActionSync } from '@spine/hook/HookActionSync';
import { Logger } from '..';

export const loggerHook = HookActionSync.template(
  (label: string, message: string, params: any[], enabled: boolean, logger: Logger, level: number) => {},
);
