import { HookActionSync } from '@spine/hook/HookActionSync';

export const loggerModeHook = HookActionSync.template(
  (label: string, mode: boolean | string) => {},
);
