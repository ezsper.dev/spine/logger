declare module "debug/src/common.js" {
  import * as debug from 'debug';
  export const setup: () => typeof debug;
  export default setup;
}