import { getLabelKey, getMaxLevelKey } from './shared';

export function getMaxLevel() {
  return process.env[getMaxLevelKey()];
}

export function load(label: string): string | undefined {
  return process.env[getLabelKey(label)];
}

export function save(label: string) {
  return (namespaces: string) => {
    const key = getLabelKey(label);
    if (namespaces) {
      process.env[key] = namespaces;
    } else {
      delete process.env[key];
    }
  };
}