export * from './hooks/logger';
export * from './hooks/loggerDisplayName';
export * from './hooks/loggerMode';
export declare const hooks: {
    logger: import("@spine/hook/HookActionSync").HookActionSync<(label: string, message: string, params: any[], enabled: boolean, logger: LoggerMember<any, any>, level: number) => void>;
    loggerDisplayName: import("@spine/hook/HookFilterSync").HookFilterSync<string, (displayName: string, parents: LoggerMember<any, any>[], path: string[], logger: LoggerMember<any, any>) => string>;
    loggerMode: import("@spine/hook/HookActionSync").HookActionSync<(label: string, mode: string | boolean) => void>;
};
export interface LogOptions<L extends LogLevels = any> {
    label: Exclude<keyof L, number | symbol>;
    message: string;
    private?: boolean;
    params?: any[];
    [key: string]: any;
}
export declare type LogLevelInput = number | {
    level: number;
    warn: boolean;
} | {
    level: number;
    error: boolean;
} | {
    level: number;
    info: boolean;
};
export interface LogLevels {
    [label: string]: LogLevelInput;
}
export declare const logLevels: {
    error: number;
    warn: number;
    special: number;
    info: number;
    verbose: number;
    debug: number;
    silly: number;
};
export interface LoggerOptions<L extends LogLevels> {
    readonly levels: {
        [K in Exclude<keyof L, number | symbol>]: number;
    };
    [key: string]: any;
}
export declare function loadDefaults(label: string): void;
export declare function getMaxLevel(): number;
export declare function load(label: string): string | undefined;
export declare function trigger(label: string, mode?: string | boolean): void;
export declare function enabled(label: string, namespace: string): boolean;
export declare function save(label: string, mode?: string | boolean): void;
export declare type LoggerMember<L extends LogLevels = any, P = any> = {
    readonly name: string;
    readonly options: LoggerOptions<L>;
    getDisplayName(): string;
    enabled(label: L): boolean;
    child<C extends LogLevels = L>(name: string, options?: LoggerOptions<C>): Log<C, Logger<L, P>>;
    log: Log<L, P>;
} & (P extends void ? {} : {
    readonly parent: P;
});
export declare type LoggerMemberMethods<L extends LogLevels> = {
    [K in Exclude<keyof L, number | symbol>]: LogExec<L>;
};
export declare type Logger<L extends LogLevels = any, P = any> = LoggerMember<L, P>;
export interface LogExecWithLevel<L extends LogLevels> {
    (label: Exclude<keyof L, number | symbol>, message: string, ...param: any[]): void;
    (label: Exclude<keyof L, number | symbol>, error: Error): void;
    (label: Exclude<keyof L, number | symbol>, options: Pick<LogOptions<L>, Exclude<keyof LogOptions<L>, 'label'>>): void;
    (options: LogOptions<L>): void;
}
export interface LogExec<L extends LogLevels> {
    (message: string, ...param: any[]): void;
    (error: Error): void;
    (options: LogOptions<L>): void;
}
export declare type Log<L extends LogLevels = any, A = any> = LogExecWithLevel<L> & LoggerMemberMethods<L> & {
    logger: Logger<L, A>;
};
export declare function logger<P extends Logger | void, O extends LoggerOptions<any> = LoggerOptions<typeof logLevels>, L extends LogLevels = O['levels']>(name: string, options?: Partial<O>, parent?: P): Log<L, P>;
