"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMaxLevel = getMaxLevel;
exports.load = load;
exports.save = save;

var _shared = require("./shared");

function getMaxLevel() {
  return process.env[(0, _shared.getMaxLevelKey)()];
}

function load(label) {
  return process.env[(0, _shared.getLabelKey)(label)];
}

function save(label) {
  return function (namespaces) {
    var key = (0, _shared.getLabelKey)(label);

    if (namespaces) {
      process.env[key] = namespaces;
    } else {
      delete process.env[key];
    }
  };
}
//# sourceMappingURL=node.js.map