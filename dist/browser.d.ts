export declare function getEnv(name: string): string | undefined;
export declare function getMaxLevel(): string | undefined;
export declare function load(label: string): string | undefined;
export declare function save(level: string): (namespaces: string) => void;
