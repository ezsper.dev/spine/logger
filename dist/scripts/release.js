"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.join");

require("core-js/modules/es.regexp.exec");

var _stringify = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/json/stringify"));

var _projectPath = require("@spine/project-path");

var _path = require("path");

var fs = _interopRequireWildcard(require("fs-extra"));

var _exec = require("../internal/utils/exec");

/**
 * This script builds to distribuition
 */
// First clear the build output dir.
(0, _exec.exec)("rimraf ".concat((0, _path.resolve)((0, _projectPath.getProjectPath)(), './dist'))); // Ignore all that is not used on client

var ignore = ["node_modules", "bin", "build", "dist", "tests", "types.d", "sample"].join(','); // Then compile to pure JS

(0, _exec.exec)("tsc --emitDeclarationOnly --skipLibCheck && babel --source-maps --ignore ".concat(ignore, " -d dist --extensions \".ts,.tsx\" .")); // Copy documents

fs.copySync((0, _path.resolve)((0, _projectPath.getProjectPath)(), 'README.md'), (0, _path.resolve)((0, _projectPath.getProjectPath)(), 'dist/README.md')); // Clear our package json for release

var packageJson = require('../package.json');

packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson.private;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;
fs.writeFileSync((0, _path.resolve)((0, _projectPath.getProjectPath)(), './dist/package.json'), (0, _stringify.default)(packageJson, null, 2));
fs.remove((0, _path.resolve)((0, _projectPath.getProjectPath)(), 'dist/internal'));
fs.copySync((0, _path.resolve)((0, _projectPath.getProjectPath)(), 'LICENSE'), (0, _path.resolve)((0, _projectPath.getProjectPath)(), 'dist/LICENSE'));
//# sourceMappingURL=release.js.map