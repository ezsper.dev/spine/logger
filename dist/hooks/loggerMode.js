"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loggerModeHook = void 0;

var _HookActionSync = require("@spine/hook/HookActionSync");

var loggerModeHook = _HookActionSync.HookActionSync.template(function (label, mode) {});

exports.loggerModeHook = loggerModeHook;
//# sourceMappingURL=loggerMode.js.map