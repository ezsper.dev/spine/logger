"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.displayNameHook = void 0;

var _HookFilterSync = require("@spine/hook/HookFilterSync");

var displayNameHook = _HookFilterSync.HookFilterSync.template(function (displayName, parents, path, logger) {
  return displayName;
});

exports.displayNameHook = displayNameHook;
//# sourceMappingURL=loggerDisplayName.js.map