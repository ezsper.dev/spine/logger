import { HookFilterSync } from '@spine/hook/HookFilterSync';
export declare const displayNameHook: HookFilterSync<string, (displayName: string, parents: import("..").LoggerMember<any, any>[], path: string[], logger: import("..").LoggerMember<any, any>) => string>;
