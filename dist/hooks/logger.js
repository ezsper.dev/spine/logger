"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loggerHook = void 0;

var _HookActionSync = require("@spine/hook/HookActionSync");

var loggerHook = _HookActionSync.HookActionSync.template(function (label, message, params, enabled, logger, level) {});

exports.loggerHook = loggerHook;
//# sourceMappingURL=logger.js.map