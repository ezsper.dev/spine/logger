import { HookActionSync } from '@spine/hook/HookActionSync';
export declare const loggerHook: HookActionSync<(label: string, message: string, params: any[], enabled: boolean, logger: import("..").LoggerMember<any, any>, level: number) => void>;
