import { HookActionSync } from '@spine/hook/HookActionSync';
export declare const loggerModeHook: HookActionSync<(label: string, mode: string | boolean) => void>;
