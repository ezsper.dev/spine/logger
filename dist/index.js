"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

require("core-js/modules/es.array.for-each");

require("core-js/modules/es.array.join");

require("core-js/modules/es.function.name");

require("core-js/modules/es.object.define-property");

require("core-js/modules/es.object.keys");

require("core-js/modules/es.regexp.constructor");

require("core-js/modules/es.regexp.exec");

require("core-js/modules/es.regexp.to-string");

require("core-js/modules/es.string.replace");

require("core-js/modules/es.string.split");

require("core-js/modules/web.dom-collections.for-each");

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  hooks: true,
  logLevels: true,
  loadDefaults: true,
  getMaxLevel: true,
  load: true,
  trigger: true,
  enabled: true,
  save: true,
  logger: true
};
exports.loadDefaults = loadDefaults;
exports.getMaxLevel = getMaxLevel;
exports.load = load;
exports.trigger = trigger;
exports.enabled = enabled;
exports.save = save;
exports.logger = logger;
exports.logLevels = exports.hooks = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/toConsumableArray"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/typeof"));

var _assign = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/assign"));

var _map = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/map"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/objectSpread"));

var _slice = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/slice"));

var _filter = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/filter"));

var _parseInt2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-int"));

var _logger = require("./hooks/logger");

Object.keys(_logger).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _logger[key];
    }
  });
});

var _loggerDisplayName = require("./hooks/loggerDisplayName");

Object.keys(_loggerDisplayName).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _loggerDisplayName[key];
    }
  });
});

var _loggerMode = require("./hooks/loggerMode");

Object.keys(_loggerMode).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _loggerMode[key];
    }
  });
});
var hooks = {
  logger: _logger.loggerHook,
  loggerDisplayName: _loggerDisplayName.displayNameHook,
  loggerMode: _loggerMode.loggerModeHook
};
exports.hooks = hooks;
var logLevels = {
  error: 0,
  warn: 1,
  special: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
};
exports.logLevels = logLevels;

function loadDefaults(label) {
  var value = load(label);

  if (typeof value === 'undefined') {
    value = '';
  }

  save(label, value);
}

function getMaxLevel() {
  var maxLevel;

  if (typeof process === 'undefined' || process.type === 'renderer' || process.browser === true || process.__nwjs) {
    maxLevel = require('./browser').getMaxLevel();
  } else {
    maxLevel = require('./node').getMaxLevel();
  }

  maxLevel = (0, _parseInt2.default)("".concat(maxLevel), 10);
  return isNaN(maxLevel) ? 2 : maxLevel;
}

function load(label) {
  if (typeof process === 'undefined' || process.type === 'renderer' || process.browser === true || process.__nwjs) {
    return require('./browser').load(label);
  }

  return require('./node').load(label);
}

function trigger(label) {
  var mode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  _loggerMode.loggerModeHook.do(label, mode);
}

var enableMap = {};
var names = {};
var skips = {};

_loggerMode.loggerModeHook.addAction('save', function (label, mode) {
  if (typeof process === 'undefined' || process.type === 'renderer' || process.browser === true || process.__nwjs) {
    require('./browser').save(label)(mode);
  } else {
    require('./node').save(label)(mode);
  }
});

_loggerMode.loggerModeHook.addAction('mode', function (label, mode) {
  var split = (typeof mode === 'string' ? mode : '').split(/[\s,]+/);
  var len = split.length;
  enableMap[label] = {};

  if (typeof skips[label] === 'undefined') {
    skips[label] = [];
  }

  if (typeof names[label] === 'undefined') {
    names[label] = [];
  }

  for (var i = 0; i < len; i += 1) {
    if (!split[i]) {
      // ignore empty strings
      continue;
    }

    var namespace = split[i].replace(/\*/g, '.*?');

    if (namespace[0] === '-') {
      skips[label].push(new RegExp("^".concat(namespace.substr(1), "$")));
    } else {
      names[label].push(new RegExp("^".concat(namespace, "$")));
    }
  }
});

function enabled(label, namespace) {
  if (enableMap[label] != null && typeof enableMap[label][namespace] !== 'undefined') {
    return enableMap[label][namespace];
  }

  if (namespace[namespace.length - 1] === '*') {
    return true;
  }

  var getState = function getState() {
    if (skips[label] != null) {
      var i;
      var len;

      for (i = 0, len = skips[label].length; i < len; i += 1) {
        if (skips[label][i].test(namespace)) {
          return false;
        }
      }

      for (i = 0, len = names[label].length; i < len; i += 1) {
        if (names[label][i].test(namespace)) {
          return true;
        }
      }
    }

    return false;
  };

  if (enableMap[label] == null) {
    enableMap[label] = {};
  }

  enableMap[label][namespace] = getState();
  return enableMap[label][namespace];
}

function save(label) {
  var mode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  trigger(label, mode);
}

function logger(name, options, parent) {
  var instance = {
    name: name,
    options: options != null ? options : {}
  };
  var levels;

  if (instance.options.levels == null) {
    instance.options.levels = levels = logLevels;
  } else {
    levels = instance.options.levels;
  }

  if (parent != null) {
    instance.parent = parent;
  }

  instance.enabled = function (label) {
    return enabled(label, instance.getDisplayName());
  };

  instance.child = function (name, options) {
    return logger(name, options == null ? instance.options : options, instance);
  };

  {
    var parents = [];
    var path = [];
    var _parent = instance.parent;

    while (_parent != null) {
      parents.push(_parent);
      path.push(_parent.name);
      _parent = _parent.parent;
    }

    path.push(name);
    var displayName = path.join(':');

    instance.getDisplayName = function () {
      return (0, _filter.default)(_loggerDisplayName.displayNameHook).call(_loggerDisplayName.displayNameHook, displayName, parents, path, instance);
    };
  }

  var log = function log() {
    var logOptions;

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    if (typeof args[1] === 'string') {
      logOptions = {
        label: args[0],
        message: args[1],
        params: (0, _slice.default)(args).call(args, 2)
      };
    } else if ((0, _typeof2.default)(args[1]) === 'object') {
      if (args[1] instanceof Error) {
        logOptions = {
          label: args[0],
          message: args[1].stack || args[1].message,
          params: []
        };
      } else {
        logOptions = (0, _objectSpread2.default)({
          label: args[0]
        }, args[1]);
      }
    } else {
      logOptions = (0, _objectSpread2.default)({}, args[0]);
    }

    var label = logOptions.label;
    var level;
    var methodFallback = undefined;
    var levelInput = levels[label];

    if (typeof levelInput === 'number') {
      level = levelInput;
    } else {
      level = levelInput.level;

      if ('warn' in levelInput) {
        methodFallback = 'warn';
      } else if ('error' in levelInput) {
        methodFallback = 'warn';
      } else if ('info' in levelInput) {
        methodFallback = 'warn';
      }
    }

    loadDefaults(label);

    if (logOptions.params == null) {
      logOptions.params = [];
    }

    var state = level <= getMaxLevel() || enabled(label, name);

    if (_logger.loggerHook.size > 0) {
      _logger.loggerHook.do(logOptions.label, logOptions.message, logOptions.params, state, instance, level);
    } else if ((typeof console === "undefined" ? "undefined" : (0, _typeof2.default)(console)) === 'object' && (typeof console[label] === 'function' || typeof console.log === 'function')) {
      var _context;

      var prefix = "".concat(instance.getDisplayName(), ": ");

      var _message = (0, _map.default)(_context = logOptions.message.split('\n')).call(_context, function (line) {
        return "".concat(prefix).concat(line);
      }).join('\n');

      if (logOptions.private !== true && state) {
        var _console;

        var method = typeof console[label] === 'function' ? label : typeof methodFallback === 'string' && typeof console[methodFallback] === 'function' ? methodFallback : 'log';

        (_console = console)[method].apply(_console, [_message].concat((0, _toConsumableArray2.default)(logOptions.params)));
      }
    }
  };

  instance.log = log;

  var _loop = function _loop(level) {
    if (levels.hasOwnProperty(level)) {
      log[level] = function () {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        return log.apply(void 0, [level].concat(args));
      };
    }
  };

  for (var level in levels) {
    _loop(level);
  }

  return (0, _assign.default)(log, {
    logger: instance
  });
}
//# sourceMappingURL=index.js.map