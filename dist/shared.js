"use strict";

require("core-js/modules/es.object.define-property");

require("core-js/modules/es.regexp.exec");

require("core-js/modules/es.string.replace");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLabelKey = exports.getMaxLevelKey = void 0;

var getMaxLevelKey = function getMaxLevelKey() {
  return 'MAX_LOG_LEVEL';
};

exports.getMaxLevelKey = getMaxLevelKey;

var getLabelKey = function getLabelKey(label) {
  if (label === 'debug' || label === 'verbose') {
    return label.toUpperCase();
  }

  return "LOG_".concat(label.replace(/[A-Z]/g, '_$1').toUpperCase());
};

exports.getLabelKey = getLabelKey;
//# sourceMappingURL=shared.js.map