export declare function getMaxLevel(): string | undefined;
export declare function load(label: string): string | undefined;
export declare function save(label: string): (namespaces: string) => void;
