"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getEnv = getEnv;
exports.getMaxLevel = getMaxLevel;
exports.load = load;
exports.save = save;

var _shared = require("./shared");

function getEnv(name) {
  var r;

  try {
    r = localStorage.getItem(name);
  } catch (error) {} // If debug isn't set in LS, and we're in Electron, try to load $DEBUG


  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = process.env[name];
  }

  return r == null ? undefined : r;
}

function getMaxLevel() {
  return getEnv((0, _shared.getMaxLevelKey)());
}

function load(label) {
  return getEnv((0, _shared.getLabelKey)(label));
}

function save(level) {
  return function (namespaces) {
    try {
      if (namespaces) {
        localStorage.setItem(level, namespaces);
      } else {
        localStorage.removeItem(level);
      }
    } catch (error) {// Swallow
      // XXX (@Qix-) should we be logging these?
    }
  };
}
//# sourceMappingURL=browser.js.map