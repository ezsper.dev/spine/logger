import { getLabelKey, getMaxLevelKey } from './shared';

export function getEnv(name: string) {
  let r;
  try {
    r = localStorage.getItem(name);
  } catch (error) {}

  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG
  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = process.env[name];
  }

  return r == null ? undefined : r;
}

export function getMaxLevel() {
  return getEnv(getMaxLevelKey());
}

export function load(label: string): string | undefined {
  return getEnv(getLabelKey(label));
}

export function save(level: string) {
  return (namespaces: string) => {
    try {
      if (namespaces) {
        localStorage.setItem(level, namespaces);
      } else {
        localStorage.removeItem(level);
      }
    } catch (error) {
      // Swallow
      // XXX (@Qix-) should we be logging these?
    }
  }
}