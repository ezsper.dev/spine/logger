export const getMaxLevelKey = () => 'MAX_LOG_LEVEL';

export const getLabelKey = (label: string) => {
  if (label === 'debug' || label === 'verbose') {
    return label.toUpperCase();
  }

  return `LOG_${label.replace(/[A-Z]/g, '_$1').toUpperCase()}`;
};