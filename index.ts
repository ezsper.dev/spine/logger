import { loggerHook } from './hooks/logger';
import { displayNameHook } from './hooks/loggerDisplayName';
import { loggerModeHook } from './hooks/loggerMode';

export * from './hooks/logger';
export * from './hooks/loggerDisplayName';
export * from './hooks/loggerMode';

export const hooks = {
  logger: loggerHook,
  loggerDisplayName: displayNameHook,
  loggerMode: loggerModeHook,
};

export interface LogOptions<L extends LogLevels = any> {
  label: Exclude<keyof L, number | symbol>;
  message: string;
  private?: boolean;
  params?: any[];
  [key: string]: any;
}

export type LogLevelInput = number | { level: number, warn: boolean } | { level: number, error: boolean } | { level: number, info: boolean };

export interface LogLevels {
  [label: string]: LogLevelInput;
}

export const logLevels = {
  error: 0,
  warn: 1,
  special: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5,
};

export interface LoggerOptions<L extends LogLevels> {
  readonly levels: { [K in Exclude<keyof L, number | symbol>]: number };
  [key: string]: any;
}

export function loadDefaults(label: string) {
  let value = load(label);
  if (typeof value === 'undefined') {
    value = '';
  }
  save(label, value);
}

export function getMaxLevel(): number {
  let maxLevel: number;
  if (typeof process === 'undefined' || (<any>process).type === 'renderer' || (<any>process).browser === true || (<any>process).__nwjs) {
    maxLevel = require('./browser').getMaxLevel();
  } else {
    maxLevel = require('./node').getMaxLevel();
  }
  maxLevel = parseInt(`${maxLevel}`, 10);
  return isNaN(maxLevel) ? 2 : maxLevel;
}

export function load(label: string): string | undefined {
  if (typeof process === 'undefined' || (<any>process).type === 'renderer' || (<any>process).browser === true || (<any>process).__nwjs) {
    return require('./browser').load(label);
  }
  return require('./node').load(label);
}

export function trigger(label: string, mode: string | boolean = true) {
  loggerModeHook.do(label, mode);
}

const enableMap: { [label: string]: { [name: string]: boolean  } } = {};
const names: { [label: string]: RegExp[] } = {};
const skips: { [label: string]: RegExp[] } = {};

loggerModeHook.addAction('save', (label, mode) => {
  if (typeof process === 'undefined' || (<any>process).type === 'renderer' || (<any>process).browser === true || (<any>process).__nwjs) {
    require('./browser').save(label)(mode);
  } else {
    require('./node').save(label)(mode);
  }
});

loggerModeHook.addAction('mode', (label, mode) => {
  const split = (typeof mode === 'string' ? mode : '').split(/[\s,]+/);
  const len = split.length;

  enableMap[label] = {};

  if (typeof skips[label] === 'undefined') {
    skips[label] = [];
  }

  if (typeof names[label] === 'undefined') {
    names[label] = [];
  }

  for (let i = 0; i < len; i += 1) {
    if (!split[i]) {
      // ignore empty strings
      continue;
    }

    const namespace = split[i].replace(/\*/g, '.*?');

    if (namespace[0] === '-') {
      skips[label].push(new RegExp(`^${namespace.substr(1)}$`));
    } else {
      names[label].push(new RegExp(`^${namespace}$`));
    }
  }
});

export function enabled(label: string, namespace: string): boolean {
  if (enableMap[label] != null && typeof enableMap[label][namespace] !== 'undefined') {
    return enableMap[label][namespace];
  }
  if (namespace[namespace.length - 1] === '*') {
    return true;
  }

  const getState = () => {
    if (skips[label] != null) {
      let i;
      let len;

      for (i = 0, len = skips[label].length; i < len; i += 1) {
        if (skips[label][i].test(namespace)) {
          return false;
        }
      }

      for (i = 0, len = names[label].length; i < len; i += 1) {
        if (names[label][i].test(namespace)) {
          return true;
        }
      }
    }
    return false;
  };

  if (enableMap[label] == null) {
    enableMap[label] = {};
  }

  enableMap[label][namespace] = getState();

  return enableMap[label][namespace];
}

export function save(label: string, mode: string | boolean = true) {
  trigger(label, mode);
}

export type LoggerMember<L extends LogLevels = any, P = any> = {
  readonly name: string;
  readonly options: LoggerOptions<L>;
  getDisplayName(): string;
  enabled(label: L): boolean;
  child<C extends LogLevels = L>(name: string, options?: LoggerOptions<C>): Log<C, Logger<L, P>>;
  log: Log<L, P>;
} & (P extends void ? {} : { readonly parent: P });

export type LoggerMemberMethods<L extends LogLevels> = {
  [K in Exclude<keyof L, number | symbol>]: LogExec<L>;
};

export type Logger<L extends LogLevels = any, P = any> = LoggerMember<L, P>;

export interface LogExecWithLevel<L extends LogLevels> {
  (label: Exclude<keyof L, number | symbol>, message: string, ...param: any[]): void;
  (label: Exclude<keyof L, number | symbol>, error: Error): void;
  (label: Exclude<keyof L, number | symbol>, options: Pick<LogOptions<L>, Exclude<keyof LogOptions<L>, 'label'>>): void;
  (options: LogOptions<L>): void;
}

export interface LogExec<L extends LogLevels> {
  (message: string, ...param: any[]): void;
  (error: Error): void;
  (options: LogOptions<L>): void;
}

export type Log<L extends LogLevels = any, A = any> = LogExecWithLevel<L> & LoggerMemberMethods<L> & {
  logger: Logger<L, A>;
};

export function logger<P extends Logger | void, O extends LoggerOptions<any> = LoggerOptions<typeof logLevels>, L extends LogLevels = O['levels']>(name: string, options?: Partial<O>, parent?: P): Log<L, P> {
  const instance: Logger<L, P> = <any>{
    name,
    options: options != null
      ? options
      : {},
  };

  let levels: { [K in Exclude<keyof L, number | symbol>]: LogLevelInput };

  if (instance.options.levels == null) {
    (<any>instance).options.levels = levels = <any>logLevels;
  } else {
    levels = instance.options.levels;
  }

  if (parent != null) {
    (<any>instance).parent = parent;
  }

  (<any>instance).enabled = (label: string) => {
    return enabled(label, instance.getDisplayName());
  };

  (<any>instance).child = (name: string, options?: LoggerOptions<L>) => {
    return logger(name, options == null ? instance.options : options, instance);
  };

  {
    const parents: any[] = [];
    const path: string[] = [];
    let parent: any = (<any>instance).parent;
    while (parent != null) {
      parents.push(parent);
      path.push(parent.name);
      parent = parent.parent;
    }
    path.push(name);
    const displayName = path.join(':');
    instance.getDisplayName = () => {
      return displayNameHook.filter(displayName, parents, path, instance);
    };
  }

  const log = (...args: any[]) => {
    let logOptions: LogOptions<L>;
    if (typeof args[1] === 'string') {
      logOptions = {
        label: args[0],
        message: args[1],
        params: args.slice(2),
      };
    } else if (typeof args[1] === 'object') {
      if (args[1] instanceof Error) {
        logOptions = {
          label: args[0],
          message: args[1].stack || args[1].message,
          params: [],
        };
      } else {
        logOptions = {
          label: args[0],
          ...args[1],
        };
      }
    } else {
      logOptions = {
        ...args[0]
      };
    }

    const label: Exclude<keyof L, number | symbol> = logOptions.label;
    let level: number;
    let methodFallback: string | undefined = undefined;
    const levelInput =  levels[label];
    if (typeof levelInput === 'number') {
      level = levelInput;
    } else {
      level = (<any>levelInput).level;
      if ('warn' in levelInput) {
        methodFallback = 'warn';
      } else if ('error' in levelInput) {
        methodFallback = 'warn';
      } else if ('info' in levelInput) {
        methodFallback = 'warn';
      }
    }

    loadDefaults(label);

    if (logOptions.params == null) {
      logOptions.params = [];
    }
    const state = level <= getMaxLevel() || enabled(<any>label, name);
    if (loggerHook.size > 0) {
      loggerHook.do(
        logOptions.label,
        logOptions.message,
        logOptions.params,
        state,
        <any>instance,
        level,
      );
    } else if (typeof console === 'object' && (typeof (<any>console)[label] === 'function' || typeof console.log === 'function')) {
      const prefix = `${instance.getDisplayName()}: `;
      const message = logOptions.message.split('\n')
        .map(line => `${prefix}${line}`)
        .join('\n');
      if (logOptions.private !== true && state) {
        const method = typeof (<any>console)[label] === 'function'
          ? label
          : (typeof methodFallback === 'string' && typeof (<any>console)[methodFallback] === 'function'
            ?  methodFallback
            : 'log'
          );
        (<any>console)[method](message, ...logOptions.params);
      }
    }
  };

  instance.log = <any>log;

  for (const level in levels) {
    if (levels.hasOwnProperty(level)) {
      (<any>log)[level] = (...args: any[]) => log(level, ...args);
    }
  }
  return <any>Object.assign(log, {
    logger: instance,
  });
}