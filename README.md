# Spine Logger Utility

Spine Logger logs your project and let users format their logs the way they want.

## Usage

#### Environment keys

##### `MAX_LOG_LEVEL`

Define the maximum log to display, default is `3`.

##### `DEBUG` | `VERBOSE` | `LOG_${LABEL}`

Defines specific log namespaces to display overwriting maximum log level

#### Basic

```
import { logger } from '@spine/logger';

export const log = logger('my-project');

log.debug('My debug message');
```

#### Custom levels

```
// LOG_FOOBAR=my-project
import { logger } from '@spine/logger';

export const log = logger('my-project', {
  levels: {
    foobar: 1,
  },
});

log.foobar('My foobar message');
// my-project[foobar]: My foobar message
```

#### Hooks

```
import { loggerHook, displayNameHook } from '@spine/logger';

displayNameHook.addFilter('your-plugin', (displayName, parents, path) => {
  return path.join('/');
});

loggerHook.addAction('your-plugin', (label, message, params, enabled, logger) => {
  if (enabled) {
    console.log(`${logger.getDisplayName()}: ${message}`, ...params);
  }
});
```