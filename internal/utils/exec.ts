import { execSync, ExecSyncOptions } from 'child_process';
import { getProjectPath } from '@spine/project-path';

export function exec(command: string, options?: ExecSyncOptions): void {
  execSync(command, {
    stdio: 'inherit',
    cwd: getProjectPath(),
    ...options,
  });
}
