/// <reference types="jest" />

import { logger, loggerHook } from '..';

describe('Basic', () => {

  it('Action trigger', () => {
    const log = logger('foo');
    let matchMessage: string | undefined;
    let matchLabel: string | undefined;
    loggerHook.addAction('ìntercept', (label, message, params, enabled, logger) => {
      if (logger === log.logger) {
        matchMessage = message;
        matchLabel = label;
      }
    });
    log.error('message');
    expect(matchMessage).toBeDefined();
    expect(matchMessage).toBe('message');
    expect(matchLabel).toBeDefined();
    expect(matchLabel).toBe('error');
  });

  it('Custom labels', () => {
    const log = logger('foo', {
      levels: {
        bar: 1,
      },
    });
    let matchMessage: string | undefined;
    let matchLabel: string | undefined;
    loggerHook.addAction('ìntercept', (label, message, params, enabled, logger) => {
      if (logger === log.logger) {
        matchMessage = message;
        matchLabel = label;
      }
    });
    log.bar('message');
    expect(matchMessage).toBeDefined();
    expect(matchMessage).toBe('message');
    expect(matchLabel).toBeDefined();
    expect(matchLabel).toBe('bar');
  });

});
